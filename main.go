package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"regexp"
	"syscall"

	"golang.org/x/term"
)

func main() {
	args := os.Args[1:]
	if len(args) != 2 {
		fmt.Printf("Tool to encrypt and decrypt file with AES-256-GCM\nEncrypt\n\taes e <filename>\nDecrypt\n\taes d <filename>\n")
		os.Exit(0)
	}
	action := args[0]
	src := args[1]
	if action == "e" {
		dst := src + ".enc"
		err := encryptFile(src, dst)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		fmt.Printf("%s encrypted to %s\n", src, dst)
	} else if action == "d" {
		var re = regexp.MustCompile(`\.enc$`)
		dst := re.ReplaceAllString(src, "")
		err := decryptFile(src, dst)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		fmt.Printf("%s decrypted to %s\n", src, dst)
	} else {
		fmt.Printf("Invalid argument\n")
	}
}

func encryptFile(src, dst string) error {
	key := getKeyFromPassword()
	bytes, err := ioutil.ReadFile(src)
	if err != nil {
		return err
	}
	encrypted, err := encrypt(bytes, key)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(dst, encrypted, 0644)
	return err
}

func decryptFile(src, dst string) error {
	key := getKeyFromPassword()
	bytes, err := ioutil.ReadFile(src)
	if err != nil {
		return err
	}
	decrypted, err := decrypt(bytes, key)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(dst, decrypted, 0644)
	return err
}

func getKeyFromPassword() []byte {
	var input []byte
	var err error
	for {
		fmt.Print("Password: ")
		input, err = term.ReadPassword(int(syscall.Stdin))
		if err != nil {
			panic(err.Error())
		}
		fmt.Printf("\n")
		if len(input) > 0 {
			break
		}
	}
	hash := sha256.Sum256(input)
	return hash[:]
}

func encrypt(content, key []byte) ([]byte, error) {
	// Create a new Cipher Block from the key
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	// Create a new GCM - https://en.wikipedia.org/wiki/Galois/Counter_Mode
	// https://golang.org/pkg/crypto/cipher/#NewGCM
	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	// Create a nonce. Nonce should be from GCM
	nonce := make([]byte, aesGCM.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, err
	}

	// Encrypt the data using aesGCM.Seal
	// Since we don't want to save the nonce somewhere else in this case, we add it as a prefix to the encrypted data. The first nonce argument in Seal is the prefix.
	encrypted := aesGCM.Seal(nonce, nonce, content, nil)
	return encrypted, nil
}

func decrypt(encrypted, key []byte) ([]byte, error) {
	// Create a new Cipher Block from the key
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	// Create a new GCM
	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}

	// Get the nonce size
	nonceSize := aesGCM.NonceSize()

	// Extract the nonce from the encrypted data
	nonce, ciphertext := encrypted[:nonceSize], encrypted[nonceSize:]

	// Decrypt the data
	plaintext, err := aesGCM.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return nil, err
	}

	return plaintext, nil
}
