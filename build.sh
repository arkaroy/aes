#!/bin/bash
platforms=(linux/amd64 linux/arm64)
for platform in ${platforms[@]}
do
    IFS='/' read -ra parts <<< "$platform"
    os=${parts[0]}
    arch=${parts[1]}
    echo Building for ${os}/${arch}
    CGO_ENABLED=0 GOOS=${os} GOARCH=${arch} go build --ldflags "-s -w" -o build/aes-${os}-${arch} .
done